/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

import Object.EnumAndConstant.AccountStatus;
import Object.EnumAndConstant.Role;

/**
 *
 * @author DAT
 */
public abstract class Account {

    private String user;
    private String password;
    private Role role;
    private AccountStatus status;
    public Person person;

    
    protected Account(String  user, Role role, Person person) {
        this.user = user;
        this.role = role;
        this.person = person;
    }

    protected Account(String user, String password, Role role, Person person) {
        this.user = user;
        this.password = password;
        this.role = role;
        this.person = person;
    }

    public abstract boolean addUser(String user, String name, String password, String role, String phone_number, String address);
    public abstract boolean updateUser(Account acc);

    
 
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

//    public String getPassword() {
//        return password;
//    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }
    
    
    
    


}
