/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

import Control.BookControl;
import Control.UserControl;
import Object.EnumAndConstant.Role;
import javax.swing.JOptionPane;

/**
 *
 * @author DAT
 */
public class Librarian extends Account {

    private Role role;

    public Librarian(String user, Person person) {
        super(user, Role.LIBRARIAN, person);
    }

    public Librarian(String user, String password, Person person) {
        super(user, password, Role.LIBRARIAN, person);
    }

    protected Librarian(String user, String password, Role role, Person person) {
        super(user, password, role, person);
    }

    protected Librarian(String user, Role role, Person person) {
        super(user, role, person);
    }

    @Override
    public boolean addUser(String user, String name, String password, String role, String phone_number, String address) {
        if (UserControl.add(user, name, password, role, phone_number, address)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean updateUser(Account acc) {
        if (UserControl.update(acc)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean addBook(Book book, Author author) {
        if (BookControl.addBook(book, author)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean updateBook(String isbn, String title, String subject, String publisher, int nop) {
        if (BookControl.updateBook(isbn, title, subject, publisher, nop)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean addBookItem(String isbn, int nob, String publicationDate, String location) {
        if (BookControl.addBookItem(isbn, nob, publicationDate, location)) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean deleteBook(String isbn) {
        if (BookControl.deleteBook(isbn)) {
            return true;
        } else {
            return false;
        }
    }
}
