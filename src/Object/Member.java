/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

import Control.UserControl;
import Object.EnumAndConstant.Role;
import javax.swing.JOptionPane;

/**
 *
 * @author DAT
 */
public class Member extends Account {

    public Member(String user, Person person) {
        super(user,Role.MEMBER, person);
    }

    public Member(String user, String password, Person person) {
        super(user, password, Role.MEMBER, person);
    }

    @Override
    public boolean addUser(String user, String name, String password, String role, String phone_number, String address) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public boolean updateUser(Account acc) {
        if (UserControl.update(acc)) {
//            JOptionPane.showMessageDialog(null, "Updating completed!");
            return true;
        } else {
//            JOptionPane.showMessageDialog(null, "Updating failed!");
            return false;
        }
    }


    
}
