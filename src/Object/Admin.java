/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Object;

import Control.UserControl;
import Object.EnumAndConstant.Role;
import javax.swing.JOptionPane;

/**
 *
 * @author DAT
 */
public class Admin extends Librarian {

    public Admin(String user, Person person) {
        super(user, Role.ADMIN, person);
    }


    public Admin(String user, String password, Person person) {
        super(user, password, Role.ADMIN, person);
    }

    public boolean deleteUser(Account acc) {
        if (UserControl.delete(acc)) {
//            System.out.println("im admin");
//            JOptionPane.showMessageDialog(null, "Deleting completed!");
            return true;
        } else {
//            JOptionPane.showMessageDialog(null, "Deleting failed!");
            return false;
        }
    }

}
