/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Object.EnumAndConstant.AccountStatus;
import Object.Person;
import Object.Account;
import Object.Admin;
import Object.EnumAndConstant;
import Object.EnumAndConstant.Role;
import Object.Librarian;
import Object.Member;
import java.awt.HeadlessException;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author DAT
 */
public class UserControl {

    private static PreparedStatement ps;
    private static ResultSet rs;
    private static Connection conn;
    private static String query;
    private static Account acc = null;

    public static Account getAccountFollowRole(String user, String password, Role role, Person person) {

        switch (role) {
            case MEMBER:
                acc = new Member(user, password, person);
                break;
            case LIBRARIAN:
                acc = new Librarian(user, password, person);
                break;
            case ADMIN:
                acc = new Admin(user, password, person);
                break;
            default:
                acc = null;
                break;
        }
        return acc;
    }

    public static Account login(String user, String password) {

        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement("SELECT * FROM account where user = ? and password = ?");
            ps.setString(1, user);
            ps.setString(2, password);
            rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String usr = rs.getString("user");
                Role role = Role.valueOf(rs.getString("role"));
                AccountStatus status = AccountStatus.valueOf(rs.getString("status"));
                Person person = new Person(rs.getString("name"), rs.getString("address"), rs.getString("phone_number"));

                acc = getAccountFollowRole(user, "", role, person);

            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {

            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);

        }

        return acc;
    }

    public static boolean add(String user, String name, String password, String role, String phone_number, String address) {
        query = "insert into account (user, name, password, role, phone_number, address) values(?,?,?,?,?,?)";

        try {
            conn = ConnectDB.getInstance().getConnect();
            ps = conn.prepareStatement(query);

            ps.setString(1, user);
            ps.setString(2, name.equals("") ? "Member" : name);
            ps.setString(3, password);
            ps.setString(4, role);
            ps.setString(5, phone_number);
            ps.setString(6, address);
            ps.execute();
            ps.close();
            JOptionPane.showMessageDialog(null, "Adding successfully");
        } catch (HeadlessException | SQLException e) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(null, "Adding failed");
            return false;
        }
        return true;
    }

    public static boolean update(Account user) {
        query = "UPDATE account SET name = ?, role = ?, phone_number = ?, address = ? where user = ?";
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);

            ps.setString(1, user.person.getName());
            ps.setString(2, user.getRole().name());
            ps.setString(3, user.person.getPhoneNumber());
            ps.setString(4, user.person.getAddress());
            ps.setString(5, user.getUser());
//            System.out.println("Giá trị ps.executeUpdate = "+ps.executeUpdate());
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public static boolean delete(Account user) {
        query = "DELETE FROM account WHERE user = ?";
        try {
            conn = ConnectDB.getInstance().getConnect();
            ps = conn.prepareStatement(query);
            ps.setString(1, user.getUser());
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            return false;
        }
    }

//    public static ArrayList<Account> getUserList() {
//        ArrayList<Account> userList = new ArrayList<>();
//        try {
//
//            Statement st = ConnectDB.getInstance().getConnect().createStatement();
//            rs = st.executeQuery("select * from account");
//
//            while (rs.next()) {
//                Account user = new Account(rs.getInt("id"),
//                        rs.getString("user"),
//                        rs.getString("password"),
//                        rs.getString("role"),
//                        rs.getInt("status"),
//                        new Person(rs.getString("name"),
//                                rs.getString("address"),
//                                rs.getString("phone_number")
//                        )
//                ) {
//                };
//                userList.add(user);
//            }
//
//        } catch (SQLException ex) {
//            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return userList;
//    }
//
//    public static void showUserList(JTable table) {
//        DefaultTableModel model = (DefaultTableModel) table.getModel();
//        ArrayList<Account> userlist = getUserList();
//        Object[] row = new Object[5];
//
//        for (int i = 0; i < userlist.size(); i++) {
//
//            row[0] = userlist.get(i).person.getName();
//            row[1] = userlist.get(i).getUser();
//            row[2] = userlist.get(i).getRole();
//            row[3] = userlist.get(i).person.getPhoneNumber();
//            row[4] = userlist.get(i).person.getAddress();
//            model.addRow(row);
//        }
//
//    }
    public static void filter(JTable table, String text) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        String query;
        if (text.trim() == null || text.trim() == "") {
            query = "SELECT * FROM account";
        } else {
            query = "SELECT * FROM account where name like '%" + text.trim() + "%' OR user like '%" + text.trim() + "%'";
        }
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Object[] row = new Object[6];
                row[0] = rs.getString("name");
                row[1] = rs.getString("user");
                row[2] = rs.getString("role");
                row[3] = rs.getString("phone_number");
                row[4] = rs.getString("address");
                row[5] = rs.getString("status");
                model.addRow(row);
            }
            ps.close();
        } catch (SQLException ex) {

            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean isExist(String inputCheck, String phoneOrUsername) {
        String phone_query = null, username_query = null;
        PreparedStatement phone_ps = null;
        PreparedStatement username_ps = null;
        ResultSet phone_rs = null;
        ResultSet username_rs = null;
        if (phoneOrUsername.equals("checkPhone")) {
            phone_query = "select count(*) as count from account where phone_number = ?";
        } else if (phoneOrUsername.equals("checkUsername")) {
            username_query = "select count(*) as count from account where user = ?";
        }
        conn = ConnectDB.getInstance().getConnect();
        try {
            phone_ps = conn.prepareStatement(phone_query);
            phone_ps.setString(1, phoneOrUsername);
            username_ps.setString(1, phoneOrUsername);

            phone_rs = phone_ps.executeQuery();
            username_rs = username_ps.executeQuery();
            while (phone_rs.next() || username_rs.next()) {
                int count = phone_rs.getInt("count") + username_rs.getInt("count");

                if (count > 0) {
                    return true;
                } else {
                    // do nothing.
                }
            }

        } catch (SQLException ex) {
            System.out.println("Error at " + UserControl.class.getName() + " isExist");
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean checkPassword(Account acc, String currentPassword) {
        query = "select count(*) as count from account where user = ? and password = ?";
        conn = ConnectDB.getInstance().getConnect();
        try {
            ps = conn.prepareStatement(query);
            ps.setString(1, acc.getUser());
            ps.setString(2, currentPassword);
            rs = ps.executeQuery();
            if (rs.next()) {
                if (rs.getInt("count") == 1) {

                    return true;
                }
            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("thất bại");
        return false;
    }

    public static boolean changePassword(Account acc, String newPassword) {
        query = "update account set password = ? where user = ?";
        conn = ConnectDB.getInstance().getConnect();
        try {
            ps = conn.prepareStatement(query);
            ps.setString(1, newPassword);
            ps.setString(2, acc.getUser());
            return ps.executeUpdate() > 0;

        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
