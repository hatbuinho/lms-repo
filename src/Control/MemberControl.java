/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import static Control.UserControl.getAccountFollowRole;
import Object.Account;
import Object.EnumAndConstant;
import Object.Member;
import Object.Person;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author chinh
 */
public class MemberControl {

    private static PreparedStatement ps;
    private static ResultSet rs;
    private static String query;
    private static Connection conn;
    private static Account acc = null;

    public static void filterMember(String text, JTextField txtMemberName, JTextField txtMemberNumber) {
//        DefaultTableModel model = (DefaultTableModel) table.getModel();
//        model.setRowCount(0);
        String query = "SELECT * FROM account where id = ?";
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            ps.setInt(1, Integer.parseInt(text));
            rs = ps.executeQuery();
            while (rs.next()) {
//                String usr = rs.getString("user");
//                EnumAndConstant.Role role = EnumAndConstant.Role.valueOf(rs.getString("role"));
//                EnumAndConstant.AccountStatus status = EnumAndConstant.AccountStatus.valueOf(rs.getString("status"));
//                Person person = new Person(rs.getString("name"), rs.getString("address"), rs.getString("phone_number"));
//                acc = getAccountFollowRole(usr, "", role, person);
                txtMemberName.setText(rs.getString("name"));
                txtMemberNumber.setText(rs.getString("phone_number"));
                break;
            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
