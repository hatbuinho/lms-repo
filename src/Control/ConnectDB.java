/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author DAT
 */
public class ConnectDB {
    /*create Singleton*/
    private static ConnectDB SINGLE_INSTANCE;
    private ConnectDB() {}
    
    public static ConnectDB getInstance() {
        if(SINGLE_INSTANCE == null) SINGLE_INSTANCE = new ConnectDB();
        return SINGLE_INSTANCE;
    }
    
    private final String connectDBString = "jdbc:mysql://localhost:3306/lms?useUnicode=yes&characterEncoding=UTF-8";
    private static Connection conn;
    public Connection getConnect() {
        
        try {
            conn = DriverManager.getConnection(connectDBString, "root", "root");
        } catch (SQLException ex) {
//            Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Kết nối database không thành công!");
//            JOptionPane.showMessageDialog(null, "Not Connected");
        }

        return conn;
    }
}
