/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Object.Author;
import Object.Book;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author DAT
 */
public class BookControl {

    private static PreparedStatement ps;
    private static ResultSet rs;
    private static String query;
    private static Connection conn;

    public static boolean addBook(Book book, Author author) {
        query = "call add_book(?,?,?,?,?,?,?)";    //(isbn title subject publisher number_of_page author_name author_ordinal)
        try {
            conn = ConnectDB.getInstance().getConnect();
            ps = conn.prepareStatement(query);
            ps.setString(1, book.getIsbn());
            ps.setString(2, book.getTitle());
            ps.setString(3, book.getSubject());
            ps.setString(4, book.getPublisher());
            ps.setInt(5, book.getNumberOfPage());
            ps.setString(6, author.getName());
            ps.setInt(7, author.getOrdinal());
            ps.execute();
            ps.close();
        } catch (SQLException ex) {
            System.out.println("error at BookControl.addBook");
            Logger.getLogger(BookControl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public static boolean updateBook(String isbn, String title, String subject, String publisher, int nop) {
        query = "call update_book(?,?,?,?,?)";   //(title subject publisher, number_of_page)
        conn = ConnectDB.getInstance().getConnect();
        try {
            ps = conn.prepareStatement(query);
            ps.setString(1, isbn);
            ps.setString(2, title);
            ps.setString(3, subject);
            ps.setString(4, publisher);
            ps.setInt(5, nop);
            ps.execute();
            ps.close();
        } catch (Exception e) {
            System.out.println("error at BookControl.updateBook");
            Logger.getLogger(BookControl.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }

        return true;
    }

    public static boolean deleteBook(String isbn) {
        query = "delete from book where isbn = ?";
        conn = ConnectDB.getInstance().getConnect();
        try {
            ps = conn.prepareStatement(query);
            ps.setString(0, isbn);
            ps.execute();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(BookControl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;
    }
    
        public static void filterBook(JTable table, String text) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        String query;
        if (text.trim() == null || text.trim() == "") {
            query = "SELECT * FROM book_with_authors limit 15";
        } else {
            query = "SELECT * FROM book_with_authors where title like '%" + text.trim() + "%' OR author like '%" + text.trim() + "%' OR isbn = '" + text.trim() + "'";
        }
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Object[] row = new Object[6];
                row[0] = rs.getString("isbn");
                row[1] = rs.getString("title");
                row[2] = rs.getString("subject");
                row[3] = rs.getString("publisher");
                row[4] = rs.getString("number_of_page");
                row[5] = rs.getString("author");
                model.addRow(row);
            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*End of book control======================================================================================*/
    public static boolean addBookItem(String isbn, int nob, String publicationDate, String location) {
        query = "call add_book_item (?,?,?,?)";

        try {
            conn = ConnectDB.getInstance().getConnect();
            ps = conn.prepareStatement(query);
            ps.setInt(1, nob);
            ps.setString(2, isbn);
            ps.setString(3, publicationDate);
            ps.setString(4, location);
            ps.execute();
            ps.close();
            conn.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("error at BookControl.addBookItem");
//            Logger.getLogger(BookControl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {

        }

    }

    public static boolean filterBookItem(JTable table, String text) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        String query;
        if (text.trim() == null || text.trim() == "") {
            query = "SELECT * FROM book_item limit 27";
        } else {
            query = "SELECT * FROM book_item_rack where isbn = '" + text.trim() + "' OR title like '%" + text.trim() + "%' limit 27";
        }
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            rs = ps.executeQuery();
            if (!rs.next()) {
                return false;
            }
            while (rs.next()) {
                Object[] row = new Object[6];
                row[0] = rs.getString("isbn");
                row[1] = rs.getString("id");
                row[2] = rs.getString("title");
                row[3] = rs.getString("publication_date");
                row[4] = rs.getString("status");
                row[5] = rs.getString("location");
                model.addRow(row);
            }
            rs.close();
            ps.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    public static boolean filterBookItemInBookLending(JTable table, int id) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        String query = "SELECT * FROM book_item where id = " + id;
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            rs = ps.executeQuery();
            if (!rs.next()) {
                return false;
            }
            while (rs.next()) {
                Object[] row = new Object[6];
                row[0] = rs.getString("isbn");
                row[1] = rs.getString("id");
                row[2] = rs.getString("title");
                row[3] = rs.getString("publication_date");
                row[4] = rs.getString("status");
                row[5] = rs.getString("location");
                model.addRow(row);
            }
            rs.close();
            ps.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static void showBookItem(JTable table, String text) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);

        query = "SELECT * FROM book_item_rack where isbn = " + text.trim();

        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Object[] row = new Object[6];
                row[0] = rs.getString("isbn");
                row[1] = rs.getString("id");
                row[2] = rs.getString("title");
                row[3] = rs.getString("publication_date");
                row[4] = rs.getString("status");
                row[5] = rs.getString("location");
                model.addRow(row);
            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void getNumberBook(String isbn,JTextField tongsoluong,JTextField noide) {
        String query;
        int bookid;
        int soluong = 0;
        String location = "";
          query = "SELECT * FROM book_item_rack where isbn = " + isbn;
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                location = rs.getString("location");
                soluong = soluong + 1;
            }
            rs.close();
            tongsoluong.setText(String.valueOf(soluong));
            noide.setText(location);
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Boolean filterLendingBookItem(String bookitemid, JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
//        model.setRowCount(0);
        String query = "call lms.get_lending_bookitem(?);";
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            ps.setInt(1, Integer.parseInt(bookitemid));
            rs = ps.executeQuery();
            while (rs.next()) {
                Object[] row = new Object[6];
                row[0] = rs.getString("id");
                row[1] = rs.getString("title");
                row[2] = rs.getString("publication_date");
                row[3] = rs.getString("publisher");
                model.addRow(row);
                break;
            }
            rs.close();
            ps.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static void createLendingBook(String memberId, ArrayList<String> bookItemId, String dueDate, String creationDate) {
        for (String i : bookItemId) {
//        for (int i =0; i < bookItemId.l; i++) {
            String query = "call lms.create_lendingbook(?,?,?,?);";
            try {
                ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
                ps.setInt(1, Integer.parseInt(memberId));
                System.out.println("mber" + memberId);
                System.out.println(Integer.parseInt(memberId));
                ps.setInt(2, Integer.parseInt(i));
                ps.setString(3, dueDate);
                ps.setString(4, creationDate);
                rs = ps.executeQuery();
                
//                ps.setDate(5, (java.sql.Date) returnDate);
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    public static void viewManageLendingBook(JTable table, int member_id, String booklending_id) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        String query;
        if (member_id != 0 && booklending_id != "" && booklending_id != null) {
            query = "SELECT * FROM book_lending where member_id = " + String.valueOf(31) + "and id = " + booklending_id;
        } else if (member_id == 0 && booklending_id != "" && booklending_id != null) {
            query = "SELECT * FROM book_lending where id = " + booklending_id;
        } else {
            query = "SELECT * FROM book_lending limit 100";
        }
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
//            ps.setString(1, "");
            rs = ps.executeQuery();
            while (rs.next()) {
                Object[] row = new Object[7];
                row[0] = rs.getString("id");
                row[1] = rs.getString("book_item_id");
                row[2] = rs.getString("member_id");
                row[3] = rs.getString("creation_date");
                row[4] = rs.getString("due_time");
                row[5] = rs.getString("return_date");
                if (row[5] == null) {
                    row[6] = "Chưa trả";
                } else {
                    row[6] = "Đã trả sách";
                }
                model.addRow(row);
            }
            rs.close();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void getNameMemberLendingBook(String memberid,JTextField nameMember) {
        String query = "SELECT * FROM account where id = ?";
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
             ps.setInt(1, Integer.parseInt(memberid));
            rs = ps.executeQuery();
            while (rs.next()) {
                String name = rs.getString("name");
                nameMember.setText(String.valueOf(name));
            }
            rs.close();
//            nameMember.setText(String.valueOf(soluong));
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Boolean getTitleLendingBook(String bookitemid, JTextField titleBook) {
//        DefaultTableModel model = (DefaultTableModel) table.getModel();
//        model.setRowCount(0);
        String query = "call lms.get_lending_bookitem(?);";
        try {
            ps = ConnectDB.getInstance().getConnect().prepareStatement(query);
            ps.setInt(1, Integer.parseInt(bookitemid));
            rs = ps.executeQuery();
            while (rs.next()) {
                String title = rs.getString("title");
                titleBook.setText(String.valueOf(title));
            }
            rs.close();
            ps.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserControl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    /*End of control book item======================================================================================*/

}
